struct base {
	char base_val;
	struct base* next;
};

void free_base(struct base*);
struct base* dec_to_base(int dec, int base);
