struct bits {
    int bit;
    struct bits *next;
};

struct bits* dec_to_bin(int dec);

void free_bits(struct bits*);
